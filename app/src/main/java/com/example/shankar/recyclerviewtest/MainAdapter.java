package com.example.shankar.recyclerviewtest;


import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by Shankar on 22-04-2017.
 */

public class MainAdapter extends RecyclerView.Adapter<MainAdapter.ViewHolder> {
    private List<Movie> movieList;
    public class ViewHolder extends RecyclerView.ViewHolder{

        public TextView mTextView1;
        public TextView mTextView2;
        public ViewHolder(View v){
            super(v);
            mTextView1=(TextView) v.findViewById(R.id.title);
            mTextView2=(TextView) v.findViewById(R.id.year);
        }
    }
    public MainAdapter(List <Movie> dataset){
        this.movieList=dataset;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v=LayoutInflater.from(parent.getContext()).inflate(R.layout.recycler_item,parent,false);

        return  new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {

        Movie movie = movieList.get(position);
        holder.mTextView1.setText(movie.getTitle());
        holder.mTextView2.setText(Integer.toString(movie.getYear()));

    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }
}
